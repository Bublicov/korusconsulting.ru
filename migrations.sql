﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: Вс 02.12.18 20:49:35
-- Версия сервера: 5.7.23-0ubuntu0.18.04.1
-- Версия клиента: 4.1
--

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE test;

--
-- Удалить таблицу `test_timesheets`
--
DROP TABLE IF EXISTS test_timesheets;

--
-- Удалить таблицу `test_users`
--
DROP TABLE IF EXISTS test_users;

--
-- Установка базы данных по умолчанию
--
USE test;

--
-- Создать таблицу `test_users`
--
CREATE TABLE test_users (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  email varchar(50) NOT NULL,
  employer_id int(11) UNSIGNED DEFAULT NULL,
  info text DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 2730,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `test_timesheets`
--
CREATE TABLE test_timesheets (
  fk_user_id int(11) NOT NULL,
  time varchar(255) NOT NULL,
  date date NOT NULL
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 2340,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE test_timesheets
ADD CONSTRAINT FK_test_timesheets_test_users_id FOREIGN KEY (fk_user_id)
REFERENCES test_users (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- 
-- Вывод данных для таблицы test_users
--
INSERT INTO test_users VALUES
(1, 'Иван', 'ivan@mail.ru', NULL, 'Текст Иван'),
(2, 'Петр', 'peter@mail.', 1, 'Текст Петр'),
(3, 'Константин', 'constantine@mail.ru', NULL, 'Текст Константин'),
(4, 'Алевтина', 'alevtina@русскоязычныйдомен.рф', 2, 'Текст Алевтина'),
(5, 'Инокентий', 'kesha@yandex.ru', 3, NULL),
(6, 'Яна', 'yana@gmail.com', 1, NULL);

-- 
-- Вывод данных для таблицы test_timesheets
--
INSERT INTO test_timesheets VALUES
(3, '2:00:00', '2017-02-10'),
(1, '5:00:00', '2017-02-15'),
(2, '1:00:00', '2017-02-11'),
(3, '4:00:00', '2017-02-12'),
(4, '1:00:00', '2017-02-13'),
(5, '1:00:00', '2017-02-13'),
(6, '2:00:00', '2017-02-13');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;