<?php

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Config
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

define('DB_HOST', 'localhost');
define('DB_USER', 'newuser');
define('DB_PASS', 'password');
define('DB_NAME', 'test');
define('DB_PORT', 3306);

define('DEFAULT_DATE_FROM', '10/02/2017');
define('DEFAULT_DATE_TO', '15/02/2017');

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* MySQL
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Доступ\запросы к БД
 */
class DB
{
	private $_connection;
	private static $_instance;

	public static function getInstance()
	{
		if (!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	private function __construct()
	{
		$this->_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT);
		$this->_connection->set_charset("utf8");

		// Error handling
		if (mysqli_connect_error()) {
			trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(),
				E_USER_ERROR);
		}
	}

	private function __clone()
	{
	}

	private function __wakeup()
	{
	}

	public function __destruct()
	{
		mysqli_close($this->_connection);
	}

	protected function executeSql($sql, $bind_params = null): array
	{
		$rows = [];
		if ($stmt = $this->_connection->prepare($sql)) {

			if (!empty($bind_params)) {
				call_user_func_array([$stmt, "bind_param"], $bind_params);
			}

			$stmt->execute();
			$result = $stmt->get_result();

			while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$rows[] = $row;
			}

			$stmt->close();
		}

		return $rows;
	}

	public function getTimeSheets($from_date, $to_date, $build_hierarchy = true): array
	{
		$sql = "SELECT * 
                  FROM test.test_users tu LEFT JOIN (
                    SELECT tt.fk_user_id, SUM(HOUR(CAST(tt.time AS time))) AS time_sum, COUNT(1) days_count
                      FROM test.test_timesheets tt
                      WHERE `date` BETWEEN STR_TO_DATE(?, '%d/%m/%Y') AND STR_TO_DATE(?, '%d/%m/%Y')
                    GROUP BY tt.fk_user_id  
                  ) t ON tu.id = t.fk_user_id
                ORDER BY tu.id";

		$rows = $this->executeSql($sql, ['ss', &$from_date, &$to_date]);

		return $build_hierarchy ? buildHierarchy($rows, 'employer_id', 'id') : $rows;
	}

	public function getImperfectionDate($user_id, $from_date, $to_date)
	{
		$sql = "SELECT HOUR(tt.time) time_on, (8 - HOUR(tt.time)) time_imperfection, `date`
                  FROM test.test_timesheets tt
                WHERE tt.fk_user_id = ?
                  AND date BETWEEN STR_TO_DATE(?, '%d/%m/%Y') AND STR_TO_DATE(?, '%d/%m/%Y')                
                  AND HOUR(tt.time) < 8
                  ORDER BY tt.date DESC";

		return $this->executeSql($sql, ['iss', &$user_id, &$from_date, &$to_date]);
	}
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Helpers
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Построить иерархию юзеров как массив
 *
 * @param $rows
 * @param $pidKey
 * @param null $idKey
 * @return mixed
 */
function buildHierarchy($rows, $pidKey, $idKey = null)
{
	$grouped = array();
	foreach ($rows as $sub) {
		$grouped[$sub[$pidKey] ?? 0][] = $sub;
	}

	$fnBuilder = function ($siblings) use (&$fnBuilder, $grouped, $idKey) {
		foreach ($siblings as $k => $sibling) {
			$id = $sibling[$idKey];
			if (isset($grouped[$id])) {
				$sibling['children'] = $fnBuilder($grouped[$id]);

				//сумма часов подчинённых
				$sibling['time_sum_employer'] = array_sum(array_map(function ($item) {
					return $item['time_sum'] ?? 0;
				}, $sibling['children']));
			}
			$siblings[$k] = $sibling;
		}

		return $siblings;
	};

	$tree = $fnBuilder($grouped[0]);

	return $tree;
}

/**
 * Вывод html для иерархии юзеров
 *
 * @param $data
 * @return string
 */
function viewHierarchy($data)
{
	$fnBuilder = function ($data, $deep = 0) use (&$fnBuilder, &$str) {
		foreach ($data as $item) {
			if (!empty($item['time_sum']) || !empty($item['time_sum_employer'])) {
				$str .= '<tr data-content="' . $item['info'] . '">
                    <td>' . str_repeat("->", $deep) . $item['name'] . '</td>
                    <td>' . $item['email'] . ' <span class="badge badge-danger">' . (!filter_var($item['email'], FILTER_VALIDATE_EMAIL) ? 'incorrect' : '') . '</span></td>
                    <td>' . $item['time_sum'] . '</td>
                    <td>' . viewPopoverImperfection($item['id'], $item['time_sum']) . '</td>            
                    <td>' . $item['time_sum_employer'] . '</td>            
                </tr>';
			}
			if (isset($item['children'])) {
				$fnBuilder($item['children'], $deep + 1);
			}
		}

		return $str;
	};

	$str = $fnBuilder($data);

	return $str;
}

/**
 * @param $user_id
 * @param $time_sum
 * @return string
 */
function viewPopoverImperfection($user_id, $time_sum)
{
	return ($time_sum % 8 == 0 ? '' : '<div class="imperfection" data-user_id="' . $user_id . '" style="cursor: pointer">Да</div>');
}

/**
 * @return null|string
 */
function getUserId(): ?string
{
	$matches = [];
	if (!empty($_POST['user_id'])) {
		$r = preg_match('/^\d+$/', $_POST['user_id'], $matches);
	}
	return current($matches) ?? null;
}

/**
 * @return null|string
 */
function getDateFrom(): ?string
{
	$matches = [];
	if (!empty($_POST['period'])) {
		$r = preg_match('/^\d{2}\/\d{2}\/\d{4}/', $_POST['period'], $matches);
	}
	return current($matches) ?: DEFAULT_DATE_FROM;
}

/**
 * @return null|string
 */
function getDateTo(): ?string
{
	$matches = [];
	if (!empty($_POST['period'])) {
		$r = preg_match('/\d{2}\/\d{2}\/\d{4}$/', $_POST['period'], $matches);
	}
	return current($matches) ?: DEFAULT_DATE_TO;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Handler Ajax
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

if (!empty($_GET['type']) && $_GET['type'] == 'ajax') {

	$rows = DB::getInstance()->getImperfectionDate(getUserId(), getDateFrom(), getDateTo());

	$html = '';
	foreach ($rows as $row) {
		$html .= ($html ? '<br>' : '') . $row['date'] . ': <br>учтено ' . $row['time_on'] . 'ч., доработка ' . $row['time_imperfection'] . 'ч.';
	}

	echo $html;
	die();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Output
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
?>


<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- fontawesome CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

    <!-- Bootstrap Datepicker -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>

    <!-- Toastr-->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


    <title>Test Case</title>
</head>
<body>

<div class="container-fluid" style="max-width: 1300px;">

    <div class="row" style="margin: 20px 0 30px 0">
        <div class="col">
            <h1 class="display-4">Тестовое задание</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-9">
            <table class="table table-sm table-hover">
                <thead>
                <tr>
                    <th scope="col">Имя</th>
                    <th scope="col">Email</th>
                    <th scope="col">Часы</th>
                    <th scope="col">Недоработка</th>
                    <th scope="col">Часы подчинённых</th>
                </tr>
                </thead>
                <tbody>
				<?php echo viewHierarchy(DB::getInstance()->getTimeSheets(getDateFrom(), getDateTo())); ?>
                </tbody>
            </table>
        </div>

        <div class="col-3" style="border: solid #ecedee; padding-top: 10px; padding-bottom: 10px;">
            <form id="search-form" method="post" action="/">
                <div class="form-group">
                    <label for="inputDate">Даты отчетного периода</label>
                    <div class="input-group">
                        <input type="text" class="form-control pull-right" id="inputDate" name="period">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Поиск</button>
            </form>
        </div>
    </div>

    <div class="row" style="margin: 20px 0 30px 0">
        <div class="col">
            <hr class="my-4">
            При формировании отчета необходимо учесть и отразить следующие показатели:
            <ul>
                <li>Иерархия подчинения</li>
                <li>Количество занесенных часов за выбранный период (у всех сотрудников нужно
                    вывести 2 значения: с учетом времени подчиненных и без)
                </li>
                <li>Валидность email-адреса (если не соответствует правилам, выделить цветом)</li>
                <li>При наведении на строку таблицы\списка необходимо выводить попап с описанием
                    сотрудника
                </li>
                <li>Если у сотрудника есть дни за которые внесено менее 8 часов, необходимо указать это
                    в отчете (вывести дополнительное поле «недоработка». При наведении выводим попап с
                    указанием дат за которые были недоработки и время фактически внесенное за эту дату)
                </li>
            </ul>

            <hr class="my-4">
            ЗАМЕЧАНИЯ:
            <ul>
                <li>по умолчанию выставлен период который охватывает все записи сотрудников</li>
                <li>в задании не сказано надо ли показывать сотрудников если по ним нет записей за выбранный период, для
                    наглядности такие сотрудни не выводятся
                </li>
                <li>в начальных данных часть сотрудников была без часов, добавлены записи что бы попали в отчёт</li>
                <li>"Часы подчинённых" посчитаны для сотрудников которые находятся в непосредственном подчинении ...
                    т.е. Алевтина это сотрудник Петра, но не Ивана
                </li>
            </ul>

        </div>
    </div>

</div>


<script type="text/javascript">
    $(function () {

        //init daterange ui
        $('#inputDate').daterangepicker({
            startDate: <?php echo "'" . getDateFrom() . "'" ?>,
            endDate: <?php echo "'" . getDateTo() . "'" ?>,
            //startDate: moment().subtract(7, "days"),
            //endDate: moment(),
            locale: {
                format: 'DD/MM/YYYY'
            }
        });

        //show popover for user row
        $('table').on('mouseenter', 'tr', function () {
            show_PopoverInfo($(this).attr('data-content'));
        });
        //show popover for imperfection
        $('.imperfection').on('mouseenter', function () {
            ajax_getImperfectionDate($(this).attr('data-user_id'));
        });
    });

    //get data for Imperfection User
    function ajax_getImperfectionDate(user_id) {
        $.ajax({
            type: 'POST',
            url: '/index.php?type=ajax',
            data: {
                user_id: user_id,
                period: $('#inputDate').val()
            },
            success: function (data) {
                show_PopoverWarning(data);
            },
            error: function () {
                alert('Error occured');
            }
        });
    }

    //show popover info for user
    function show_PopoverInfo(data) {
        if (data) {
            toastr.success(data)
        }
    }

    //show popover warning for user
    function show_PopoverWarning(data) {
        if (data) {
            toastr.warning(data)
        }
    }

</script>

</body>
</html>
